package com.petservice.service;

import java.util.List;
import java.util.Set;

import com.petservice.exception.PetException;
import com.petservice.model.Pet;
import com.petservice.model.User;

public interface PetService {

	public abstract Pet savePet(Pet pet) throws PetException;

	public abstract List<Pet> getAllPets() throws PetException;

	public abstract Pet getpetById(long id) throws PetException;

	public abstract Pet buyPet(User user, long petId) throws PetException;

	public abstract Pet updatePet(Pet pet) throws PetException;

	public abstract int deletePetById(long id) throws PetException;

	public abstract Set<Pet> getAllPetsByUserId(long userId) throws PetException;
}
