package com.petservice.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petservice.exception.PetException;
import com.petservice.model.Pet;
import com.petservice.model.User;
import com.petservice.service.PetService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pets")
@Slf4j
public class PetController {

	@Autowired
	private PetService petService;

	@PostMapping(value = "/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody Pet pet) throws PetException {
		pet = petService.savePet(pet);
		if (pet != null) {
			log.info("Pet created successfully");
			return new ResponseEntity<Pet>(pet, HttpStatus.CREATED);
		}

		return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/pets")
	public ResponseEntity<List<Pet>> petHome() throws PetException {
		List<Pet> pets = petService.getAllPets();
		if (pets != null) {
			log.info("All pets found");
			return new ResponseEntity<List<Pet>>(pets, HttpStatus.OK);
		}
		return new ResponseEntity<List<Pet>>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/petDetail/{petId}")
	public ResponseEntity<Pet> petDetail(@PathVariable("petId") long id) throws PetException {
		Pet pet = petService.getpetById(id);
		if (pet != null) {
			log.info("Pet found with the id: " + id);
			return new ResponseEntity<Pet>(pet, HttpStatus.OK);
		}
		return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/allPetsByUserId/{userId}")
	public ResponseEntity<Set<Pet>> myPets(@PathVariable("userId") long userId) throws PetException {
		Set<Pet> pets = petService.getAllPetsByUserId(userId);
		if (pets != null) {
			log.info("All pets found under the user id: " + userId);
			return new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
		}
		return new ResponseEntity<Set<Pet>>(HttpStatus.NOT_FOUND);
	}

	@PutMapping(value = "updatePet")
	public ResponseEntity<Pet> updatepetDetails(@RequestBody Pet pet) throws PetException {
		Pet dbPet = petService.updatePet(pet);
		if (dbPet != null) {
			log.info("Pet updated successfully");
			return new ResponseEntity<Pet>(dbPet, HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping(value = "deletePet/{petId}")
	public ResponseEntity<String> deletePetById(@PathVariable("petId") long id) throws PetException {
		String message = null;
		int val = petService.deletePetById(id);
		if (val == 1) {
			message = "Pet deleted";
			log.info("Pet deleted successfully");
			return new ResponseEntity<String>(message, HttpStatus.GONE);
		}
		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}

	@PutMapping(value = "/buyPet/{petId}")
	public ResponseEntity<Pet> buyPet(@PathVariable long petId, @RequestBody User user) throws PetException {
		Pet pet = petService.buyPet(user, petId);
		if (pet != null) {
			log.info("Pet bought for the user id: " + user.getId());
			return new ResponseEntity<Pet>(pet, HttpStatus.OK);
		}
		return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
	}
}
