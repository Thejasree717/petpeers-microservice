package com.petservice.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petservice.model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {

	Set<Pet> findByOwner_Id(long userId);

}
