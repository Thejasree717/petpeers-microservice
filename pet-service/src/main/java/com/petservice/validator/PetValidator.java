package com.petservice.validator;

import com.petservice.model.Pet;

public interface PetValidator {

	public abstract boolean validate(Pet pet);
}
