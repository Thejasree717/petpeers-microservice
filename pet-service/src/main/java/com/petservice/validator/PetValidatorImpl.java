package com.petservice.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.petservice.model.Pet;

@Component
public class PetValidatorImpl implements PetValidator {

	private static final Logger LOGGER = LogManager.getLogger(PetValidatorImpl.class);

	@Override
	public boolean validate(Pet pet) {
		if (pet.getAge() > 0 && pet.getAge() < 100) {
			LOGGER.info("Pet created");
			return true;
		} else {
			LOGGER.info("Pet age should be between 1 and 99");
			return false;
		}

	}

}
