package com.userservice.validator;

import com.userservice.model.User;

public interface UserValidator {

	public boolean validateUser(User user);
}
