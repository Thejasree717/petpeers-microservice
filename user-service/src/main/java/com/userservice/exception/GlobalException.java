package com.userservice.exception;

import java.io.FileNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice   //AOP
public class GlobalException {

	//handle the exception
		@ExceptionHandler({MyAppException.class, FileNotFoundException.class})
		protected ResponseEntity<String> handleException(Exception exception){
			return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		@ExceptionHandler(NullPointerException.class)
		protected ResponseEntity<String> handle2Exception(Exception exception){
			return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
		}
}
