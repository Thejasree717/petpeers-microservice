package com.userservice.exception;

public class MyAppException extends Exception{

	private String message;

	public MyAppException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		
		return this.message;
	}

	
}
