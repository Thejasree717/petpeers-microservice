package com.userservice.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.userservice.dto.PetDto;
import com.userservice.exception.MyAppException;
import com.userservice.model.Pet;
import com.userservice.model.User;

@FeignClient(name = "PET-SERVICE/pets")
public interface PetClient {

	@PostMapping(value = "/addPet")
	public abstract Pet addPet(@RequestBody Pet pet) throws MyAppException;

	@GetMapping(value = "/pets")
	public abstract List<Pet> petHome() throws MyAppException;

	@GetMapping(value = "/petDetail/{petId}")
	public abstract Pet petDetail(@PathVariable("petId") long id) throws MyAppException;

	@GetMapping(value = "/getPet/{userId}")
	public abstract Pet getPetByUserId(@PathVariable("userId") long id) throws MyAppException;

	@PutMapping(value = "updatePet")
	public Pet updatepetDetails(@RequestBody Pet pet) throws MyAppException;

	@DeleteMapping(value = "deletePet/{petId}")
	public String deletePetById(@PathVariable("petId") long id) throws MyAppException;

	@PutMapping(value = "/buyPet/{petId}")
	public PetDto buyPetByUserId(@PathVariable(value = "petId") long petId, @RequestBody User user)
			throws MyAppException;

	@GetMapping(value = "/allPetsByUserId/{userId}")
	public List<PetDto> getAllPetByUserId(@PathVariable("userId") long userId);

}
