package com.userservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userservice.exception.MyAppException;
import com.userservice.model.User;
import com.userservice.repository.UserRepository;
import com.userservice.validator.UserValidator;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserValidator userValidator;

	@Autowired
	private PetClient userClient;

	@Override
	public User addUser(User user) throws MyAppException {
		User user1 = null;
		boolean is = false;
		is = userValidator.validateUser(user);
		if (is == true) {
			user1 = userRepository.save(user);
		}
		if (user1 == null) {
			throw new MyAppException("User is not added");
		}
		return user1;
	}

	@Override
	public List<User> listUsers() throws MyAppException {
		List<User> users = userRepository.findAll();
		if (users == null) {
			throw new MyAppException("Users not found");
		}
		return users;
	}

	@Override
	public User buyPet(long userId, long petId) throws MyAppException {
		User user = userRepository.findById(userId).get();
		if (user == null) {
			throw new MyAppException("User data doesn't exist");
		}
		userClient.buyPetByUserId(petId, user);
		return user;
	}

	@Override
	public int removeUser(long userId) throws MyAppException {
		User user = null;
		Optional<User> optional = userRepository.findById(userId);
		if (optional.isPresent()) {
			user = optional.get();

			if (user.getPets().size() == 0) {
				userRepository.deleteById(userId);
			} else {
				throw new MyAppException("User is not deleted");
			}
		}
		return 0;
	}

	@Override
	public User login(String userName, String userPassword) throws MyAppException {
		User user = userRepository.findByUserName(userName, userPassword);
		if (user.getUserName().equals(userName) && user.getUserPassword().equals(userPassword)) {
			return user;
		} else {
			throw new MyAppException("No user found with the given username and password");
		}
	}

	@Override
	public boolean validation(String userName, String userPassword) throws MyAppException {
		boolean result = false;
		int lengthOfUserName = userName.length();
		int lengthOfPassword = userPassword.length();
		if (lengthOfUserName >= 5 && lengthOfPassword >= 6) {
			result = true;
		}
		if (result != false) {
			return result;
		} else {
			throw new MyAppException("Validation failed please give valid userName and password");
		}

	}

}
