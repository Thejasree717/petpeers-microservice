package com.userservice.service;

import java.util.List;

import com.userservice.exception.MyAppException;
import com.userservice.model.User;

public interface UserService {

	public abstract User addUser(User user) throws MyAppException;

	public abstract List<User> listUsers() throws MyAppException;

	public abstract int removeUser(long userId) throws MyAppException;

	public User login(String userName, String userPassword) throws MyAppException;

	public boolean validation(String userName, String userPassword) throws MyAppException;

	public abstract User buyPet(long userId, long petId) throws MyAppException;

}
