package com.userservice.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.userservice.model.User;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Component
public class PetDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private String name;

	private int age;

	private String place;

	private User user;

}
