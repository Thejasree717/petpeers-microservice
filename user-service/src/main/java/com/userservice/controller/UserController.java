package com.userservice.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userservice.exception.MyAppException;
import com.userservice.model.Pet;
import com.userservice.model.User;
import com.userservice.service.UserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/add")
	public ResponseEntity<User> addUser(@Valid @RequestBody User user) throws MyAppException {
		user = userService.addUser(user);
		if (user != null) {
			log.info("User created successfully");
			return new ResponseEntity<User>(user, HttpStatus.CREATED);
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);

	}

	@GetMapping(value = "/users")
	public ResponseEntity<List<User>> listUsers() throws MyAppException {
		List<User> users = userService.listUsers();
		if (users != null) {
			log.info("List of users found");
			return new ResponseEntity<List<User>>(users, HttpStatus.OK);
		}
		return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping(value = "/delete/{userId}")
	public ResponseEntity<Integer> deleteUser(@PathVariable("userId") long userId) throws MyAppException {
		int val = userService.removeUser(userId);
		if (val == 1) {
			log.info("User deleted with id: " + userId);
			return new ResponseEntity<Integer>(val, HttpStatus.OK);
		}
		return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/login/{userName}/{userPassword}")
	public ResponseEntity<User> login(@PathVariable String userName, @PathVariable String userPassword)
			throws MyAppException {
		User user = null;
		if (userName != null && userPassword != null) {
			boolean validation = userService.validation(userName, userPassword);
			if (validation == true) {
				user = userService.login(userName, userPassword);
				if (user != null) {
					log.info("Hi " + user.getUserName() + " Welcome to HCL");
					return new ResponseEntity<User>(user, HttpStatus.FOUND);
				}
			}
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}

}
