package com.userservice.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PET_USER")
@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 5, nullable = false, name = "ID")
	private long id;

	@NotEmpty(message = "please enter user name")
	@Size(min = 4, max = 16)
	@Column(name = "USER_NAME", length = 55, nullable = false, unique = true)
	private String userName;

	@NotEmpty(message = "Please enter user password")
	@Size(min = 4, max = 16)
	@Column(length = 55, nullable = false)
	private String userPassword;

	@NotEmpty(message = "Please enter confirm password")
	@Size(min = 4, max = 16)
	@Transient
	private String confirmPassword;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
	private Set<Pet> pets;

}
